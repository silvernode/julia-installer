# julia-installer
A small, simple, bash script for installing the Julia language

##Running

`
chmod +x julia-installer.sh
`  
`
sudo ./julia-installer.sh
`  
The script will try to detect whether you have a 32-bit or 64-bit system and download the correct version of Julia.  
julia-installer directly effects your filesystem so keep that in mind, there is no container.  
Julia's files get coppied to their respective system folders and you can reverse this by choosing "uninstall".
