#!/bin/bash

VERSION="0.3.8"
DOWN_DIR="/tmp"
ARC=$(getconf LONG_BIT)
PACKAGE64="julia-${VERSION}-linux-x86_64.tar.gz"
PACKAGE32="julia-${VERSION}-linux-i686.tar.gz"

if [[ $EUID -ne 0 ]]; then
   echo "You are not root!" 1>&2
   exit 1
fi

downLoad()
{

  if [ $ARC = "64" ];then
    wget https://julialang.s3.amazonaws.com/bin/linux/x64/0.3/julia-${VERSION}-linux-x86_64.tar.gz -P ${DOWN_DIR}

  elif [ $ARC = "32" ];then
    wget https://julialang.s3.amazonaws.com/bin/linux/x86/0.3/julia-${VERSION}-linux-i686.tar.gz -P ${DOWN_DIR}
  fi

}

extrPack()
{
  if [ -f ${DOWN_DIR}/${PACKAGE64} ];then
    tar xvzf ${DOWN_DIR}/${PACKAGE64} -C ${DOWN_DIR}

  elif [ -f ${DOWN_DIR}/${PACKAGE32} ];then
    tar xvzf ${DOWN_DIR}/${PACKAGE32} - C ${DOWN_DIR}
  fi

}

installJulia()
{
    cd ${DOWN_DIR}/julia-79599ada44
    cp -v -R bin/* /usr/bin/
    cp -v -R etc/* /etc/
    echo "hi"
    cp -v -R include/* /include/
    cp -v -R lib/* /lib
    cp -v -R share/* /usr/share
    cd ${DOWN_DIR}/julia-79599ada44
    cp -v -R bin/* /usr/bin/
    cp -v -R etc/* /etc/
    cp -v -R include/* /include/
    cp -v -R lib/* /lib
    cp -v -R share/* /usr/share

}

removeJulia()
{
  rm -v -R /usr/bin/julia
  rm -v -R /usr/bin/julia-debug
  rm -v -R /usr/include/julia
  rm -v -R /usr/lib/julia
  rm -v -R /etc/julia
  rm -v -R /usr/share/julia
}
main()
{
  clear
  echo "=========================="
  echo "   JULIA INSTALLER"
  echo "=========================="
  echo
  echo "[1] - Install"
  echo "[2] - Remove"
  echo
  echo "[q] - Quit"
  echo
  echo -n "-> "
  read choice

  if [ ${choice} = "1" ];then
    downLoad
    extrPack
    installJulia
    echo
    echo -n "Would you like to run Julia now (y/n)?: "
    read runjul

    if [ ${runjul} = "y" ];then
      julia
      exit 0;
    elif [ ${runjul} = "n" ];then
      exit 0;
    fi

  elif [ ${choice} = "2" ];then
    removeJulia
    exit 0;
  elif [ ${choice} = "q" ];then
    exit 0;
  fi
}

main
